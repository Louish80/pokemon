<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\RatioType $ratioType
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Ratio Type'), ['action' => 'edit', $ratioType->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Ratio Type'), ['action' => 'delete', $ratioType->id], ['confirm' => __('Are you sure you want to delete # {0}?', $ratioType->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Ratio Types'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Ratio Type'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="ratioTypes view large-9 medium-8 columns content">
    <h3><?= h($ratioType->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($ratioType->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Attack Type Id') ?></th>
            <td><?= $this->Number->format($ratioType->attack_type_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Defend Type Id') ?></th>
            <td><?= $this->Number->format($ratioType->defend_type_id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ratio') ?></th>
            <td><?= $this->Number->format($ratioType->ratio) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($ratioType->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($ratioType->modified) ?></td>
        </tr>
    </table>
</div>
