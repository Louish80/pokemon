<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\RatioType $ratioType
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Ratio Types'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="ratioTypes form large-9 medium-8 columns content">
    <?= $this->Form->create($ratioType) ?>
    <fieldset>
        <legend><?= __('Add Ratio Type') ?></legend>
        <?php
            echo $this->Form->control('attack_type_id');
            echo $this->Form->control('defend_type_id');
            echo $this->Form->control('ratio');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
