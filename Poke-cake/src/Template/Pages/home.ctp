
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('home.css') ?>
    <link href="https://fonts.googleapis.com/css?family=Raleway:500i|Roboto:300,400,700|Roboto+Mono" rel="stylesheet">
</head>
<body class="home">
<header class="row">
    <div class="header-image"><?= $this->Html->image('/webroot/img/logo.png') ?></div>
    <div class="header-title">
        <h1>Attrapez les tous !</h1>
    </div>
</header>
<div class="header-image">
        <img class="large-row-4 medium-2" src="https://www.smashbros.com/assets_v2/img/fighter/lucario/main.png" alt="Image de lucario" />
    </div>