<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Fights Controller
 *
 * @property \App\Model\Table\FightsTable $Fights
 *
 * @method \App\Model\Entity\Fight[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FightsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Dresseurs', 'FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs']
        ];
        $fights = $this->paginate($this->Fights);

        $this->set(compact('fights'));
    }

    /**
     * View method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fight = $this->Fights->get($id, [
            'contain' => ['Dresseurs', 'FirstDresseurs', 'SecondDresseurs', 'WinnerDresseurs']
        ]);

        $this->set('fight', $fight);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fight = $this->Fights->newEntity();
        if ($this->request->is('post')) {
            $formData = $this->request->getData();
            if ($formData['first_dresseur_id'] != $formData['second_dresseur_id'])
            {
                $cbt = $this->_retrieveFightWinner($formData);
                $formData['winner_dresseur_id'] = $cbt['id'];
                $formData['fight_log'] = $cbt['log'];
                $fight = $this->Fights->patchEntity($fight, $formData);
                if ($this->Fights->save($fight)) {
                    $this->Flash->success(__('The fight has been saved.'));

                    return $this->redirect(['action' => 'view/'.$this->getFight($formData)]);
                }
                $this->Flash->error(__('The fight could not be saved. Please, try again.'));
            }
            else
            {
                $this->Flash->error(__('A fight against yourself is not possible. Please choose a different opponent.'));
            }
        }
        $dresseurs = $this->Fights->Dresseurs->find('list', ['limit' => 200]);
        $firstDresseurs = $this->Fights->FirstDresseurs->find('list', ['limit' => 200]);
        $secondDresseurs = $this->Fights->SecondDresseurs->find('list', ['limit' => 200]);
        $winnerDresseurs = $this->Fights->WinnerDresseurs->find('list', ['limit' => 200]);
        $this->set(compact('fight', 'dresseurs', 'firstDresseurs', 'secondDresseurs', 'winnerDresseurs'));
    }
    protected function _retrieveFightWinner($formData)
    {
        /*
        //Ancienne méthode moche
        $alea = random_int(0, 1);
        if ($alea == 0)
        {
            $id = $formData['first_dresseur_id'];
        }
        else
        {
            $id = $formData['second_dresseur_id'];
        }
        */
        $trainer1 = array('ID'=>$formData['first_dresseur_id'],"pokes"=>$this->getPokes($formData['first_dresseur_id']));
        $trainer2 = array('ID'=>$formData['second_dresseur_id'],"pokes"=>$this->getPokes($formData['second_dresseur_id']));
        if (count($trainer1['pokes']) == 0 && count($trainer2['pokes']) == 0)
        {
            $alea = random_int(0, 1);
            if ($alea == 0)
            {
                $cbt['id'] = $formData['first_dresseur_id'];
            }
            else
            {
                $cbt['id'] = $formData['second_dresseur_id'];
            }
            $cbt['log'] = "Aucun Dresseur n'a de pokémons, on tire au hasard le gagnant.\n";
        }
        else
        {
            if (count($trainer1['pokes']) == 0 || count($trainer2['pokes']) == 0)
            {
                if (count($trainer1['pokes']) == 0)
                    $cbt['id'] = $formData['second_dresseur_id'];
                else
                    $cbt['id'] = $formData['first_dresseur_id'];
                $cbt['log'] = "L'un des dresseurs n'a de pokémons, on considère qu'il abandonne.\n";
            }
            else
            {
                $cbt = $this->fight($trainer1, $trainer2);
            }
        }
        $winner = $this->getTrainer($cbt['id']);
        $cbt['log'] = $cbt['log']."Le gagnant est ".$winner['last_name'].' '.$winner['first_name'].'.';
        return $cbt;
    }

    protected function getFight($param)
    {
        $query = TableRegistry::getTableLocator()->get('Fights')->find()->where(['first_dresseur_id =' => $param['first_dresseur_id']])->where(['second_dresseur_id =' => $param['second_dresseur_id']])->where(['winner_dresseur_id =' => $param['winner_dresseur_id']])->where(['fight_log =' => $param['fight_log']]);
        foreach ($query as $data) {
            return $data['id'];
        }
    }

    protected function getTrainer($id_trainer)
    {
        $query = TableRegistry::getTableLocator()->get('Dresseurs')->find()->where(['id =' => $id_trainer]);
        foreach ($query as $data) {
            $dat = [
                "first_name" => $data['first_name'],
                "last_name" => $data['last_name']
            ];
            return $dat;
        }
    }

    protected function getPokes($id_trainer)
    {
        $output = array();
        $output_id = array();
        $outputtemp = array();
        $compt = 0;
        $query = TableRegistry::getTableLocator()->get('DresseurPokes')->find()->select(['poke_id', 'is_fav'])->where(['dresseur_id =' => $id_trainer]);
        foreach ($query as $data) {
            if ($data['is_fav'] == true)
            {
                $outputtemp = array();
                $outputtemp[0] = $data['poke_id'];
                for ($i = 1; $i <= count($output_id); $i++)
                {
                    $outputtemp[$i] = $output_id[$i - 1];
                }
                $output_id = $outputtemp;
            }
            else
            {
                $output_id[$compt] = $data['poke_id'];
            }
            $compt++;
        }
        for ($i = 0; $i < count($output_id); $i++)
        {
            $query = TableRegistry::getTableLocator()->get('Pokes')->find()->select(['name', 'attack', 'speed', 'health', 'defense', 'type_id'])->where(['id =' => $output_id[$i]]);
            foreach ($query as $data){
                $poke = [
                    "name" => $data['name'],
                    "HP" => $data['health'],
                    "ATT" => $data['attack'],
                    "DEF" => $data['defense'],
                    "Speed" => $data['speed'],
                    "Type" => $data['type_id']
                ];
                $output[$i] = $poke;
            }
        }
        return $output;
    }

    protected function getTypeTable()
    {
        $typetable = array();
        $query = TableRegistry::getTableLocator()->get('PokemonTypes')->find()->select(['id']);
        foreach ($query as $types) {
            $second_query = TableRegistry::getTableLocator()->get('RatioTypes')->find()->select(['defend_type_id', 'ratio'])->where(['attack_type_id =' => $types['id']]);
            foreach ($second_query as $data) {
                $typetable[$types][$data['defend_type_id']] = $data['ratio'];
            }
        }
        return $typetable;
    }
    
    protected function getTypeRatio($attacker, $defenser)
    {
        $result = TableRegistry::getTableLocator()->get('RatioTypes')->find()->select(['ratio'])->where(['attack_type_id =' => $attacker])->where(['defend_type_id =' => $defenser]);
        foreach ($result as $retour)
            return $retour['ratio'];
        return 1;
    }
    protected function crit($difficulty)
    {
        $output = array();
        if($difficulty == 0)
        {
            $output['ratio'] = 1.5;
            $output['log'] = "Coup Critique !\n";
        }
        else
        {
            if($difficulty < 0)
            $difficulty = -$difficulty;

            if(random_int(0, $difficulty) == 0)
            {
                $output['ratio'] = 1.5;
                $output['log'] = "Coup Critique !\n";
            }
            else
            {
                $output['ratio'] = 1;
                $output['log'] = "";
            }
        }
        return $output;
    }

    protected function damage($attacker, $defenser)
    {
        /*
        //V1 très lourde pour le proco
        $table = $this->getTypeTable();

        return ($attacker['ATT'] / $defenser['DEF'] + 2) * $this->crit(16) * $table[$attacker['Type']][$defenser['Type']];
        */
        $output = array();
        $crit = $this->crit(16);
        $ratio = $this->getTypeRatio($attacker['Type'],$defenser['Type']);
        if ($ratio > 1)
        {
            $output['log'] = "C est super efficace !\n";
        }
        else
        {
            if ($ratio < 1)
            {
                if ($ratio > 0)
                {
                    $output['log'] = "Ce n'est pas tres efficace...\n";
                }
                else
                {
                    $output['log'] = "Cela n'a aucun effet.\n";
                }
            }
            else
            {
                $output['log'] = "";
            }
        }
        $output['log'] = $output['log'].$crit['log'];
        if($defenser['DEF'] > 0)
        {
            $output['dgt'] = ($attacker['ATT'] / $defenser['DEF'] + 2) * $crit['ratio'] * $ratio;
        }
        else
        {
            $output['dgt'] = $attacker['ATT'] * $crit['ratio'] * $ratio;
        }
        return $output;
    }

    protected function attack($first, $second)
    {
        $out = array();
        $out['res'] = 0;

        $out['log'] = $first['name']." attaque ".$second['name'].".\n";
        $att = $this->damage($first, $second);
        $second['HP'] -= $att['dgt'];
        $out['log'] = $out['log'].$att['log'];

        if($second['HP'] <= 0)
        {
            $out['log'] = $out['log'].$second['name']." est K.O.\n";
            $out['res']++;
        }
        else
        {
            $out['log'] = $out['log'].$second['name']." attaque ".$first['name'].".\n";
            $att = $this->damage($second, $first);
            $first['HP'] -= $att['dgt'];
            $out['log'] = $out['log'].$att['log'];

            if($first['HP'] <= 0)
            {
                $out['log'] = $out['log'].$first['name']." est K.O.\n";
                $out['res']++;
            }
        }
        $out['first'] = $first;
        $out['second'] = $second;

        return $out;
    }

    protected function turn($poke1, $poke2)
    {
        $out = array();
        $out['res']= 0;

        $order = $poke1['Speed'] - $poke2['Speed'];

        if($order >= 0)
        {
            $out_ = $this->attack($poke1, $poke2);
            $poke1 = $out_['first'];
            $poke2 = $out_['second'];
        }
        else
        {
            $out_ = $this->attack($poke2, $poke1);
            $poke2 = $out_['first'];
            $poke1 = $out_['second'];
        }
        $out['res'] = $out_['res'];
        if($poke2['HP'] <= 0)
            $out['res']++;

        $out['log'] = $out_['log'];
        $out['poke1'] = $poke1;
        $out['poke2'] = $poke2;

        return $out;
    }
    protected function fight($trainer1, $trainer2)
    {
        $poke1 = 0;
        $poke2 = 0;
        $trainer_name1 = $this->getTrainer($trainer1['ID']);
        $trainer_name2 = $this->getTrainer($trainer2['ID']);
        $output['log'] = "";
        while($trainer1['pokes'][count($trainer1['pokes'])-1]['HP'] >= 0 && $trainer2['pokes'][count($trainer2['pokes'])-1]['HP'] >= 0)
        {
            $res = $this->turn($trainer1['pokes'][$poke1], $trainer2['pokes'][$poke2]);
            $output['log'] = $output['log'].$res['log'];
            $trainer1['pokes'][$poke1] = $res['poke1'];
            $trainer2['pokes'][$poke2] = $res['poke2'];
            if($res['res'] >= 2)
            {
                $res['res'] -= 2;
                $poke2++;
                if ($trainer1['pokes'][count($trainer1['pokes'])-1]['HP'] >= 0 && $trainer2['pokes'][count($trainer2['pokes'])-1]['HP'] >= 0)
                    $output['log'] = $output['log'].$trainer_name2['first_name']." envoie ".$trainer2['pokes'][$poke2]['name'].".\n";
            }
            if($res['res'] == 1)
            {
                $poke1++;
                if ($trainer1['pokes'][count($trainer1['pokes'])-1]['HP'] >= 0 && $trainer2['pokes'][count($trainer2['pokes'])-1]['HP'] >= 0)
                    $output['log'] = $output['log'].$trainer_name1['first_name']." envoie ".$trainer1['pokes'][$poke1]['name'].".\n";
            }
        }
        if($trainer1['pokes'][count($trainer1['pokes'])-1]['HP'] >= 0)
        {
            $output['id'] = $trainer1['ID'];
        }
        else
        {
            $output['id'] = $trainer2['ID'];
        }
        return $output;
    }


    /**
     * Delete method
     *
     * @param string|null $id Fight id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fight = $this->Fights->get($id);
        if ($this->Fights->delete($fight)) {
            $this->Flash->success(__('The fight has been deleted.'));
        } else {
            $this->Flash->error(__('The fight could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}