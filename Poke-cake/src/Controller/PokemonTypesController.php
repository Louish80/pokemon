<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PokemonTypes Controller
 *
 * @property \App\Model\Table\PokemonTypesTable $PokemonTypes
 *
 * @method \App\Model\Entity\PokemonType[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PokemonTypesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $pokemonTypes = $this->paginate($this->PokemonTypes);

        $this->set(compact('pokemonTypes'));
    }

    /**
     * View method
     *
     * @param string|null $id Pokemon Type id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pokemonType = $this->PokemonTypes->get($id, [
            'contain' => []
        ]);

        $this->set('pokemonType', $pokemonType);
    }

    /**
     * Edit method
     *
     * @param string|null $id Pokemon Type id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pokemonType = $this->PokemonTypes->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pokemonType = $this->PokemonTypes->patchEntity($pokemonType, $this->request->getData());
            if ($this->PokemonTypes->save($pokemonType)) {
                $this->Flash->success(__('The pokemon type has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The pokemon type could not be saved. Please, try again.'));
        }
        $this->set(compact('pokemonType'));
    }
}
