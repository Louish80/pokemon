<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;

/**
 * ReturnPokemons shell command.
 */
class ReturnPokemonsShell extends Shell
{
    /**
     * Manage the available sub-commands along with their arguments and help
     *
     * @see http://book.cakephp.org/3.0/en/console-and-shells.html#configuring-options-and-generating-help
     *
     * @return \Cake\Console\ConsoleOptionParser
     */
    public function getOptionParser()
    {
        $parser = parent::getOptionParser();
        $parser->addOption('random', ['short' => 'r', 'help' => 'Return A Random Poke', 'default' => true, 'boolean' => true]);

        return $parser;
    }

    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $query = TableRegistry::getTableLocator()->get('Pokes')->find()->select(['name', 'pokedex_number', 'health', 'attack', 'defense', 'speed', 'type_id']);
        $this->verbose('Appel à notre base de données');
        $poke_data = array();
        $compt = 0;
        foreach ($query as $data)
        {
            $type_query = TableRegistry::getTableLocator()->get('PokemonTypes')->find()->select(['name'])->where(['id =' => $data['type_id']]);
            //$data['id_type'];
            $poke_data[$compt] = $data;
            foreach ($type_query as $data)
                $poke_data['type'] = $data;
            $compt++;
        }
        if ($this->params['random'] ==  true)
        {
            $this->quiet($poke_data[random_int(0, $compt)]);
        }
        else
        {
            $this->quiet($poke_data);
        }
    }
}
