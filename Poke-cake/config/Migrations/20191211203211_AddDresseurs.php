<?php
use Migrations\AbstractMigration;

class AddDresseurs extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        // inserting multiple rows
        $rows = extract_csv('config/Migrations/csv/trainers.csv');

        $this->table('dresseurs')->insert($rows)->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        
    }
}
