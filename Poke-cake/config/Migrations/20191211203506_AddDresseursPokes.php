<?php
use Migrations\AbstractMigration;

class AddDresseursPokes extends AbstractMigration
{
    /**
     * Migrate Up.
     */
    public function up()
    {
        // inserting multiple rows
        $rows = extract_csv('config/Migrations/csv/pokemontrainer.csv');

        $this->table('dresseur_pokes')->insert($rows)->save();
    }

    /**
     * Migrate Down.
     */
    public function down()
    {
        
    }
}
